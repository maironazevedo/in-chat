function deletePost(id) {
    selectedPost = document.getElementById(`post-${id}`);
    selectedPost.remove();
}

function editPost(id) {

    selectedPost = document.getElementById(`post-${id}`);
    let editSection = document.createElement("div");
    editSection.classList.add("editContainer");
    editSection.innerHTML = `<form action="#">
    <fieldset>
    <h2>Janela de edição</h2>
    <label for="newName">Nome: </label>
    <input type="text" id="newName" required>
    <label for="newMessage">Mensagem: </label>
    <input type="text" id="newMessage" required>
    <button id="confirm" type="reset">Editar</button>
    <button id="cancel" type="reset">Cancelar</button>
    </fieldset>
    </form>`;

    selectedPost.appendChild(editSection);

    const cancelButton = document.getElementById("cancel");
    cancelButton.addEventListener("click", () => {
        const editSection = selectedPost.querySelector(".editContainer");
        editSection.remove();
    })

    const confirmButton = document.getElementById("confirm");
    confirmButton.addEventListener("click", () => {
        const editSection = selectedPost.querySelector(".editContainer");
        let newName = document.getElementById("newName").value;
        let newMessage = document.getElementById("newMessage").value;

        let oldNameContainer = selectedPost.querySelector(".username");
        let oldMessageContainer = selectedPost.querySelector(".message");

        oldNameContainer.innerHTML = `${newName}`;
        oldMessageContainer.innerHTML = `${newMessage}`;

        editSection.remove();
    })
}

const postButton = document.getElementById("postar");
let counter = 0;

postButton.addEventListener("click", () => {
    const user = document.getElementById("name").value;
    const message = document.getElementById("message").value;

    let newPost = document.createElement("div");
    newPost.setAttribute("id", `post-${counter}`);
    newPost.setAttribute("class", "newPost");

    newPost.innerHTML = `<div class="buttons">
    <button class="delete-button" onclick="deletePost(${counter})"></button>
    <button class="edit-button" onclick="editPost(${counter})"></button>
    </div>
    <div class="container">
    <h2 class="username">${user}</h2>
    <p class="message">${message}</p>
    </div>`;

    const feed = document.getElementById("feed");
    feed.appendChild(newPost);

    counter++;
})

const deleteAllButton = document.querySelector(".deleteAll");
deleteAllButton.addEventListener("click", () => {
    document.getElementById("feed").innerHTML = " ";
})